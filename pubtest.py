#!/usr/bin/env python3
import zmq
import json
from random import randrange
import time

conn_str = "tcp://localhost:1234"

ctx = zmq.Context()
s = ctx.socket(zmq.PUB)
s.connect(f'{conn_str}')

devs = ['a', 'b', 'c']
msrmnts = ['data', 'log']

while True:
    for dev in devs:
        for msr in msrmnts:
            tmst = time.time()
            if msr is 'data':
                value = randrange(0, 10)
                payload = json.dumps({'tmst': tmst, 'fields': {'a1': value, 'x1': value/2}})
            else:
                payload = json.dumps({'tmst': tmst, 'msg': 'test log message.'})
            s.send_string(f'{dev}-{msr} {payload}')
    time.sleep(1.1)
