#!/usr/bin/env python3
import sys, zmq, os, random, json

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import qt_layout

import matplotlib
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import pandas as pd

from pprint import pprint
from time import sleep

class zmqListener(QObject):

    message = pyqtSignal(str)
    
    def __init__(self):
       
        QObject.__init__(self)
        
        ctx = zmq.Context()
        self.socket = ctx.socket(zmq.SUB)
        self.socket.connect('tcp://127.0.0.1:6003')
        #self.socket.bind('tcp://*:1234')

        self.poller = zmq.Poller()
        self.poller.register(self.socket, zmq.POLLIN)

        self.running = True
    
    def add_sub(self, topic):
        self.socket.set(zmq.SUBSCRIBE, topic.encode())
        #print(f'sub: {topic}')

    def del_sub(self, topic):
        self.socket.set(zmq.UNSUBSCRIBE, topic.encode())
        #print(f'unsub: {topic}')

    def loop(self):
        while self.running:
            socks = dict(self.poller.poll(100))
            if socks:
                msg = self.socket.recv_string()
                self.message.emit(msg)

class MonitorApp(QMainWindow, qt_layout.Ui_MainWindow):
    def __init__(self, parent=None):
        super(MonitorApp, self).__init__(parent)
        self.setupUi(self)

        ctx = zmq.Context()
        self.s = ctx.socket(zmq.REQ)

        self.addSub.clicked.connect(self.add_sub)
        self.delSub.clicked.connect(self.del_sub)

        self.zmqThread = QThread()
        self.zeromq_listener = zmqListener()
        self.zeromq_listener.moveToThread(self.zmqThread)

        self.zmqThread.started.connect(self.zeromq_listener.loop)
        self.zeromq_listener.message.connect(self.data_process)

        QTimer.singleShot(0, self.zmqThread.start)

        self.timer = QTimer(self)
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.on_draw)

        self.create_mpl_frame()
        self.data = {}
        self.axes = {}

    def set_timer(self):
        if self.timer_cb.isChecked():
            self.timer.start()
        else:
            self.timer.stop()

    def add_sub(self):
        if self.devName.text():
            sub = self.devName.text()
            if self.devMsrmnt.text():
                sub = sub + ' ' + self.devMsrmnt.text()
            self.subList.addItem(f'{sub}')
            self.zeromq_listener.add_sub(sub)

    def del_sub(self):
        selected = self.subList.selectedItems()
        for item in selected:
            self.subList.takeItem(self.subList.row(item))
            topic = item.text()
            self.zeromq_listener.del_sub(topic)

    def data_process(self, msg):
        dev, msr, raw = msg.split(' ', 2)
        print(dev, msr)

        try:
            payload = json.loads(raw)
        except:
            print('failed loading payload')
            return

        df_tmp = pd.DataFrame(payload['fields'], [payload['time']])
        df_tmp.index = pd.to_datetime(df_tmp.index)

        if dev not in self.data.keys():
            self.data[dev] = df_tmp
        else:
            self.data[dev] = self.data[dev].append(df_tmp)


    def on_draw(self):
        """ Redraw the figure
        """
        # clear the axes and redraw the plot anew
        #
        self.fig.clf()
        rowcnt = 1
        nrows = len(self.data.keys())
        for d in self.data.keys():
            self.axes[d] = self.fig.add_subplot(nrows, 1, rowcnt)
            self.axes[d].grid(self.grid_cb.isChecked())
            rowcnt+=1

            for col in list(self.data[d]):
                self.axes[d].plot(self.data[d].index, self.data[d][col])

        self.fig.autofmt_xdate()
        self.canvas.draw()

    def create_mpl_frame(self):
        self.mpl_frame = QWidget(self.mplWidget)

        # Create the mpl Figure and FigCanvas objects. 
        # 5x4 inches, 100 dots-per-inch
        #
        self.dpi = 100
        self.fig = Figure((5.0, 4.0), dpi=self.dpi, tight_layout=True)
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self.mpl_frame)
        #
        # Since we have only one plot, we can use add_axes 
        # instead of add_subplot, but then the subplot
        # configuration tool in the navigation toolbar wouldn't
        # work.
        #
        #self.axes = self.fig.add_subplot(111)
        #
        # Bind the 'pick' event for clicking on one of the bars
        #
        #self.canvas.mpl_connect('pick_event', self.on_pick)
        #
        # Create the navigation toolbar, tied to the canvas
        #
        self.mpl_toolbar = NavigationToolbar(self.canvas, self.mpl_frame)
        #
        # Other GUI controls
        # 
        self.draw_button = QPushButton("&Draw")
        self.draw_button.clicked.connect(self.on_draw)
        
        self.grid_cb = QCheckBox("Show &Grid")
        self.grid_cb.setChecked(False)
        self.grid_cb.stateChanged.connect(self.on_draw)

        self.timer_cb = QCheckBox("Auto &Update")
        self.timer_cb.setChecked(False)
        self.timer_cb.stateChanged.connect(self.set_timer)
        
        #
        # Layout with box sizers
        # 
        hbox = QHBoxLayout()
        
        for w in [ self.draw_button, self.grid_cb, self.timer_cb]:
            hbox.addWidget(w)
            hbox.setAlignment(w, Qt.AlignVCenter)
        
        vbox = QVBoxLayout()
        vbox.addWidget(self.canvas)
        vbox.addWidget(self.mpl_toolbar)
        vbox.addLayout(hbox)
        
        self.mpl_frame.setLayout(vbox)
        #self.setCentralWidget(self.mpl_frame)

    def closeEvent(self, event):
        self.zeromq_listener.running = False
        self.zmqThread.quit()
        self.zmqThread.wait()

def main():
    app = QApplication(sys.argv)
    form = MonitorApp()
    form.show()
    app.exec_()

if __name__ == '__main__':
    main()
